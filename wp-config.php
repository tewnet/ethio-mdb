<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'Ethio-MDB');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'K?M:y^i}k1FbN(mg5sy2@OUS&{AeGgq1P ;m~nQvykGfpZRYxIDa^H!B1+Y(MNU;');
define('SECURE_AUTH_KEY',  'h~!]J[9LQltYAU1_L g]1)*ZAmA$%jHg1xZXdTKk]oC8>K_35qM[dy+,#UZe}9;[');
define('LOGGED_IN_KEY',    'SnB=Z&6! F7@T)Ry*F7lwFu}j3<i;+!dh]W?$RDG8%@/AMso?2c<;?Z=}~@:8v1#');
define('NONCE_KEY',        '$TraAs4/t,GXFCJpev+V{m67+g4TSc(qrF<Crd>-2{&dAK&B#.mCrim,iiiHq+sV');
define('AUTH_SALT',        'w}00obo<mJ(X~j@R=cP?b6IO9SCyH7tjlxO5@S~>GNP1%%XRd?.N2%ZY=Y^$LIjT');
define('SECURE_AUTH_SALT', ',IQy^G*[n@<ly3DO3>geZm*|m&$@D8+uutq36/h=L](]k wFil9Qm&PQLO{> ,cY');
define('LOGGED_IN_SALT',   'HTz9l9^)R(RyT~~OGQ2+7uUOz]nts2MdW:)DR.Ux/>J|;BnnG$pToI&01;p7 ~NM');
define('NONCE_SALT',       '0E(X*c6)a]8syfuAlUHHTkZ6)o19x+DWf&HT[$*+H_CMV,(x_#i[o$=xRnTqwt}S');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
